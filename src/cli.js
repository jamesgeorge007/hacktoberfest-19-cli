const {
	showBanner,
	showHelpInfo,
	showVersionInfo,
	showInvalidFlagsInfo
} = require('./helpers');

const [, , ...args] = process.argv; // eslint-disable-line unicorn/no-unreadable-array-destructuring

module.exports = async () => {
	await showBanner();
	if (['-h', '--help'].includes(args[0]) || !args.length) { // eslint-disable-line unicorn/explicit-length-check
		showHelpInfo();
	} else if (['-v', '--version'].includes(args[0])) {
		showVersionInfo();
	} else if (args[0].startsWith('-')) {
		showInvalidFlagsInfo(args);
	} else if (args[0] === 'start') {
		require('./commands/start')();
	} else if (args[0] === 'next') {
		require('./commands/next')();
	} else {
		console.log();
		console.log(' No arguments are expected');
	}
};
