const {readFileSync, writeFileSync, existsSync} = require('fs');
const {green, yellow, red} = require('kleur');
const open = require('open');

const {showBanner, waitForConfirmation} = require('../helpers');
const tasks = require('../workspace/level-one');

module.exports = async () => {
	if (!existsSync('./data.json')) {
		console.log(red(` data.json not found`));
		process.exit(1);
	}

	await waitForConfirmation();

	const result = JSON.parse(readFileSync('./data.json'));
	result.taskCount += 1;

	if (result.taskCount >= 4) {
		await showBanner();
		console.log();
		console.log(green(' Congratulations! All tasks completed'));
    console.log();

    await open('https://github.com/madlabsinc/mevn-cli');
    await open('https://github.com/madlabsinc/teachcode');
    await open('http://madhacks.madlabs.xyz');

    console.log(green(` We maintain couple of OSS projects and would love to have your contributions`));
    console.log();
    console.log(green(` Head over to ${yellow('https://github.com/madlabsinc')}`));
    console.log(green(` Also, be sure to check out ${yellow('http://madhacks.madlabs.xyz')}`));

		process.exit(1);
	}

	console.log(yellow('\t'.repeat(8) + `progress: ${result.taskCount + 1 } / 4`));
	console.log();

	console.log(green(tasks[result.taskCount].task));

	writeFileSync('./data.json', JSON.stringify(result));
};
