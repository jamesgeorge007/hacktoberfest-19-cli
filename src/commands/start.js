const {writeFileSync} = require('fs');
const {green, red, yellow} = require('kleur')
const open = require('open');
const {
	showBanner,
	showInputPrompt,
	showPasswordPrompt,
	showListPrompt,
	waitForConfirmation
} = require('../helpers');
const data = require('../data');


const showInitialPrompts = async () => {
	// Prompt for the unique access code
	const {userInput} = await showInputPrompt('Enter the code:-', true);

	if (data.findIndex(entry => entry.CODE === userInput) === -1) {
		console.log(red(' Invalid input'));
		process.exit(1);
	}

	// Prompt for year of study
	const {yearOfStudy} = await showListPrompt('Year of study:-', ['First', 'Second', 'Third', 'Fourth']);
	// Return values as an array
	return [userInput, yearOfStudy];
};

const saveDetails = (obj) => {
  writeFileSync('./data.json', JSON.stringify(obj));
};

const showWelcomeMessage = (keyCode, yearOfStudy) => {
	let path = '../workspace/';
	switch(yearOfStudy) {
		case 'First':
			path += 'level-one';
			break;
		case 'Second':
			path += 'level-two';
			break;
		case 'Third':
			path += 'level-three';
			break;
		case 'Fourth':
			path+= 'level-four';
			break;
	}
	const tasks = require(path);
	const idx = data.findIndex(entry => entry.CODE === keyCode);
  const name = data[idx].Name;
  console.log();
  console.log(green(` Welcome ${name}`));
  saveDetails({userName: `${name.split(' ')[0]}`, name, yearOfStudy, taskCount: 0});
	console.log();
	console.log(yellow('\t'.repeat(8) + 'progress: 1 / 4'));
	console.log();
  console.log(green(tasks[0].task));
};

const validateChoice = (userInput, yearOfStudy) => {
	return (userInput.includes('HACK1') && yearOfStudy === 'First' ||
    userInput.includes('HACK2') && yearOfStudy === 'Second' ||
    userInput.includes('HACK3') && yearOfStudy === 'Third' ||
    userInput.includes('HACK4') && yearOfStudy === 'Fourth');
};

module.exports = async () => {
	let [userInput, yearOfStudy] = await showInitialPrompts();

	while (!validateChoice(userInput, yearOfStudy)) {
		await showBanner();
    console.log();
    console.log(red(' Invalid choice'));
		[userInput, yearOfStudy] = await showInitialPrompts();
	}

	await waitForConfirmation();

  showWelcomeMessage(userInput, yearOfStudy);
  await open('https://github.com/mad-hacks/hacktoberfest-2k19');
  console.log();
  console.log(green(` Head over to ${yellow('https://github.com/mad-hacks/hacktoberfest-2k19')}`));
};
