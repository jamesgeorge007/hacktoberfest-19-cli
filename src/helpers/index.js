const defineBannerInfo = require('node-banner');
const inquirer = require('inquirer');
const {red} = require('kleur');
const {version} = require('../../package.json');

const showBanner = async () => {
	await defineBannerInfo('Hacktoberfest', 'CLI for Hacktoberfest-AJCE 2k19');
};

const showHelpInfo = () => {
	console.log();
	console.log(' Usage: hb [command] [options]');
	console.log();
	console.log(' hb -h | --help');
	console.log(' hb -v --version');
};

const showVersionInfo = () => {
	console.log();
	console.log(` v${version}`);
};

const showInvalidFlagsInfo = flags => {
	console.log();
	console.log(red(` Invalid flag ${flags.join()}`));
};

const waitForConfirmation = async () => {
	let result = await showPasswordPrompt('Enter the ADMIN passcode:-');

	while (result.keyCode !== 'hack@19') {
		await showBanner();
		result = await showPasswordPrompt('Enter the ADMIN passcode:-');
	}
};

const showInputPrompt = message => {
	return inquirer.prompt({
		name: 'userInput',
		message,
		type: 'input',
		validate: input => {
			if (!input) {
				return 'required input';
			}

			return true;
		}
	});
};

const showPasswordPrompt = message => {
	return inquirer.prompt({
		name: 'keyCode',
		message,
		type: 'password',
		validate: input => {
			if (!input) {
				return 'required input';
			}

			return true;
		}
	});
};

const showListPrompt = (message, choices) => {
	return inquirer.prompt({
		name: 'yearOfStudy',
		message,
		choices,
		type: 'list'
	});
};

module.exports = {
	showBanner,
	showHelpInfo,
	showVersionInfo,
	showInvalidFlagsInfo,
	showInputPrompt,
	showListPrompt,
	showPasswordPrompt,
	waitForConfirmation
};
