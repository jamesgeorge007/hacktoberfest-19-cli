# hacktoberfest-19-cli

CLI for [Hacktoberfest-AJCE](http://hacktoberfest-ajce.surge.sh).

## Usage

`hack [command] [options]`

`hack start` - kick start

`hack next` - fetch next task